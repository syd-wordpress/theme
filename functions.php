<?php


function remove_post_custom_fields() {
    remove_meta_box( 'postcustom' , 'post' , 'normal' );
}
add_action( 'admin_menu' , 'remove_post_custom_fields' );






function my_remove_meta_boxes() {
    if ( ! current_user_can( 'manage_options' ) ) {
        remove_meta_box( 'project_category', 'project', 'normal' );
        remove_meta_box( 'project_tag', 'project', 'normal' );
        remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');

    }
}
add_action( 'admin_menu', 'my_remove_meta_boxes' );




function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}

function force_fullwidth_doc($ID = false, $post = false)
{
    if($post && $post->post_type != 'post')
        return;

    // force full width !!!
    update_post_meta( $ID, '_et_pb_page_layout', 'et_full_width_page');

}

add_action('save_post_project', 'force_fullwidth_doc');



function custom_post_name() {
    register_post_type( 'project',
        array(
            'labels' => array(
                'name' => __( 'Projets CSE', 'divi' ),
                'singular_name' => __( 'Projet CSE', 'divi' ),
            ),
            'has_archive' => false,
            'menu_icon' => 'dashicons-portfolio',
            'hierarchical' => true,
            'public' => true,
            'rewrite' => array( 'slug' => 'projet-cse', 'with_front' => false ),
            'supports' => array(),
        ));
}
add_action( 'init', 'custom_post_name' );




function isDark($hex)
{
    // Format the hex color string
    $hex = str_replace('#', '', $hex);
    if (strlen($hex) == 3) {
        $hex = str_repeat(substr($hex, 0, 1), 2) . str_repeat(substr($hex, 1, 1), 2) . str_repeat(substr($hex, 2, 1), 2);
    }
    // Get decimal values
    $r = hexdec(substr($hex, 0, 2));
    $g = hexdec(substr($hex, 2, 2));
    $b = hexdec(substr($hex, 4, 2));

    //get luma
    $l = (0.2126 * $r + 0.7152 * $g + 0.0722 * $b) / 255;

    if ($l > 0.5)
        return false;
    else
        return true;
}


function custom_excerpt_length( $length ) {
        return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



  //--------------------------------------------------------------
  //--------------------------------------------------------------

  
function include_css() {
	
	// enqueue style
	// enqueue parent styles
	wp_enqueue_style('Divi', get_template_directory_uri() .'/style.css', 20);
	
	// enqueue child styles
	wp_enqueue_style('Syd', get_stylesheet_directory_uri() .'/assets/css/main.css', 1);

}
add_action('wp_enqueue_scripts', 'include_css', 99);


  //--------------------------------------------------------------
  //--------------------------------------------------------------
/* Import class blog rejoignez nous */

function wpb_hook_javascript() {
	if (is_page ('25563')) { 
	  ?>
		  <script type="text/javascript">
			window.addEventListener('DOMContentLoaded', () =>{
				document.querySelectorAll('.et_pb_post').forEach(el => el.classList.add("custom_offres","et_pb_module","et_pb_toggle", "et_pb_toggle_close", "et_pb_toggle_0", "et_pb_toggle_item", "background_blog", "line_under_post"));
				document.querySelectorAll('.entry-title').forEach(el => el.classList.add("et_pb_toggle_title", "offres_title"));
				document.querySelectorAll('.entry-title').forEach(el => el.classList.remove("entry-title"));
				document.querySelectorAll('.post-content-inner').forEach(el => el.classList.add("et_pb_toggle_content", "clearfix"));
			});
			
			
		</script>
	  <?php
	}
  }
  add_action('wp_head', 'wpb_hook_javascript');



  //--------------------------------------------------------------
  //--------------------------------------------------------------
  // ------ SHORTCODE FOR PAGE fonds-de-dotation-caritatif-societal-et-environnemental-c-s-e/



  add_shortcode( 'clients', 'display_clients' );

  function display_clients($atts){


    extract( shortcode_atts( array (
        'nombre' => 4,
        'border' => 'no',
        'infos' => 'yes',
    ), $atts ) );


    if($nombre == 'all'){
        $nombre = -1;
    }

    $args = array(
        'post_type' => 'project',
        'post_status' => 'publish',
        'posts_per_page' => $nombre,
        'orderby' => 'post_date',
        'order' => 'DESC',

    );

    $string = '';
    $query = new WP_Query( $args );
    if( $query->have_posts() ){

        $string = '<div class="clients">';

        while( $query->have_posts() ){
            $query->the_post();



            if($border == 'yes'){
                $string .= '<div class="reference-item with-border">';
            }else{
                $string .= '<div class="reference-item no-border">';
            }
            $string .= '<div class="titre">'.get_the_title().'</div>';
            $string .= '<div class="description">'.get_the_excerpt().'</div>';
            $string .= '<div class="image"><img src="'.get_the_post_thumbnail_url().'" /></div>';

            if($infos == 'yes'){
                $string .= '<div class="overlay-references">';
                $string .= '<span class="description">"'.get_the_content().'"</span>';
                $string .= '<span class="title">'.get_field('client', get_the_ID()).'</span>';
                $string .= '</div>';
            }else{

            }
            $string .= '</div>';
        }

    }

    $string .= '</div>';
    $string .= '<div style=clear:both;></div>';
    wp_reset_postdata();
    return $string;
}




?>