class Website{

    constructor(){

    }

    init(){

        console.log('Website::init');

        // Auto Init
        let dragger = new Dragger({
            scroller : $('.scroller'),
            stack    : $('.archives__cards'),
            debug    : true
        });

        let stack = new Stack();
        stack.init();

        // Text d'eading sur le logo
        //CSSPlugin.defaultTransformPerspective = 600
        //let s = document.querySelectorAll('.logo__title span')
        //TweenMax.staggerFromTo(s, 1, { y : 20, opacity:0, z: 500, rotationY: 100}, {rotationY: 0, z: 0, transformOrigin: '0 100%', y:0, opacity:1, delay: 1, ease:Back.easeOut.config(5)}, 0.04)

        TweenMax.staggerFromTo('.hero__skill', 0.4, { y : 20, opacity:0, z: 500}, {z: 0, transformOrigin: '0 100%', y:0, opacity:1, delay: 1 }, 0.04);

        // Carousel
        $('.js-carousel').slick({
            slidesToShow: 1,
            adaptiveHeight: false,
            infinite: true,
            lazyLoad: 'ondemand',
            prevArrow: '<button type="button" class="slick-prev"><span></span></button>',
            nextArrow: '<button type="button" class="slick-next"><span></span></button>'
        });

        // Filters
        let filters = new Filters();
        filters.init();

        // ScrollTo
        let pageNav = new PageNav();
        pageNav.init();

        // Contact
        let contact = new Contact();
        contact.init();

        // Mecanos
        let mecanos = new Mecanos();
        mecanos.init();

        // HOme Slider
        let gearslider = new GearSlider();
        gearslider.init();


        let coords = [-1.56123360, 47.2129207];
        mapboxgl.accessToken = 'pk.eyJ1IjoiYXVyZWxpZW5wZXRpdGdhcmFnZSIsImEiOiJjajMxczNrcGYwMDAzMndxa2lrMDA2Z2xsIn0.u8ITIdoLVB7NFppzLHpqVw';
        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/aurelienpetitgarage/cj32sx6y200032so0tkderwkw',
            zoom: 15,
            center: coords
        });

        map.on('load', () => {
            map.loadImage('http://lpgv2.lpg/wp-content/themes/LPG/assets/img/lpg.png', (error, image) => {
                if (error) throw error;
                map.addImage('logo', image);
                map.addLayer({
                    "id": "points",
                    "type": "symbol",
                    "source": {
                        "type": "geojson",
                        "data": {
                            "type": "FeatureCollection",
                            "features": [{
                                "type": "Feature",
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": coords
                                }
                            }]
                        }
                    },
                    "layout": {
                        "icon-image": "logo"
                    }
                });
            });
        });

    }

}


// Entry point
document.addEventListener('DOMContentLoaded', function(){

    const website = new Website();
    website.init();

});



