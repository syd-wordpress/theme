<?php get_header(); ?>
 
<div id="main-content">
    <div class="container">
        <div id="content-area" class="clearfix">
            <div id="">
                <article id="post-0" <?php post_class( 'et_pb_post not_found' ); ?>>
                    <h1>Aucun résultat</h1>
                    <p>La page demandée est introuvable. Essayez d'affiner votre recherche ou utilisez le panneau de navigation ci-dessus pour localiser l'article.</p>
                </article> <!-- .et_pb_post -->
            </div> <!-- #left-area -->
 
        </div> <!-- #content-area -->
    </div> <!-- .container -->
</div> <!-- #main-content -->
 
<?php get_footer(); ?>
