<!-- FOOTER -->
<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<!-- footer container -->
<?php endif;
if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<div class="footer_container">

				<div class="footer_wrapper">

		      		<div class="footer_info">
		      			<div class="footer_title">SYD Nantes</div>
		      			<p>
		      			(siège – centre de services)<br/>
						11 rue de la Rabotière<br/>
						44800 Saint Herblain<br/>
						E-mail : contact@syd.fr 
						</p>
						<p>
						Tél. 02 51 13 51 13<br/>
						Fax. 02 51 13 51 12
						</p>
					</div>
			      	<div class="footer_info">
			      		<div class="footer_title">SYD Paris</div>
			      		<p>
			      		3 rue Maître Jacques<br/>
						92100 Boulogne Billancourt<br/>
						</p>
						<p>
						Tél. 01 85 73 39 99
						</p>
					</div>
			      	<div class="footer_info">
			      		<div class="footer_title">SYD Brest</div>
			      		<p>
			      		2 quai de la douane<br/>
						29200 Brest
						</p>
						<p>
						Tél. 07 81 92 35 62
						</p>
					</div>
			      	<div class="footer_info">
			      		<div class="footer_title">SYD Niort</div>
			      		<p>
			      		10 place du pilori<br/>
						79000 Niort
						</p>
					</div>

				</div>

			</div>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix" style="text-align:center;">
						Copyright © 2017 Syd Groupe. All Rights Reserved. <a href="/mentions-legales" style="font-style:italic;color:#408ab9;">Mentions légales</a>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>